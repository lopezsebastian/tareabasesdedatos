<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Untitled</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="author" href="humans.txt">
    </head>
    <body>
    	<h1>Actualizar datos</h1>        
		<?php
			if (!empty($_GET['id'])) {
       			$id = $_GET['id'];
	       		include 'coneccion.php';
				$link = conect();
				$stringQuery = "SELECT * FROM usuario WHERE id = $id";
				// para buscar con referencia a una cadena de texto se busca la acdena dentro de ''
				$sql = mysqli_query($link, $stringQuery);
				$row = mysqli_fetch_assoc($sql);
				$name = utf8_encode($row['nombre']);
				$email = utf8_encode($row['correo']);
				?>
				<form id="frmFormulario" name="frmFormulario" method="post" action="actualizar-usuarios.php">
					<table>
						<tr>
							<th><label for="txtId"> Id</label></th>
							<td><input type="text" name="txtId" id="txtId" readonly="" value="<?php echo $id?>"></td>
						</tr>
						<tr>
							<th><label for="txtNombre"> Nombre </label></th>
							<td><input type="text" name="txtNombre" id="txtNombre" value="<?php echo $name?>"></td>
						</tr>
						<tr>
							<th><label for="mail"> Correo </label></th>
							<td><input type="email" name="mail" id="mail" value="<?php echo $email?>"></td>
						</tr>
						<tr>
							<td colspan="2">
		                        <input type="submit" value="Guardar cambios" />
		                        <input type="reset" value="Restablecer" />
		                    </td>
						</tr>
					</table>
				</form>
		<?php
			}else{
       			$message = "Debe elegir un registro a eliminar";
       			echo "<script>alert('$message'); window.location='mostrar_usuario.php';</script>";
       		}
		?>
        <script src="js/main.js"></script>
    </body>
</html>
